using CustomSelectables;
using SimpleUi.Abstracts;
using UnityEngine;

namespace Runtime.Game.Ui.Windows.StartToPlay
{
    public class StartToPlayView : UiView
    {
        public CustomButton StartToPlay;
        
        [SerializeField] public RectTransform Fillers;
        [SerializeField] public CustomButton Filler1Btn;
        [SerializeField] public CustomButton Filler2Btn;
        [SerializeField] public RectTransform Filler1Enabled;
        [SerializeField] public RectTransform Filler2Enabled;
    }
}