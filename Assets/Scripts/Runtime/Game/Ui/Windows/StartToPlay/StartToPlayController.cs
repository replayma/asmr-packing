using DG.Tweening;
using ECS.Game.Components.Flags;
using ECS.Game.Systems.GameCycle;
using ECS.Utils.Extensions;
using ECS.Views.GameCycle;
using Leopotam.Ecs;
using Runtime.Services.AnalyticsService;
using SimpleUi.Abstracts;
using UniRx;
using UnityEngine;
using Utils.UiExtensions;
using Zenject;

namespace Runtime.Game.Ui.Windows.StartToPlay 
{
    public class StartToPlayController : UiController<StartToPlayView>, IInitializable
    {
        [Inject] private IAnalyticsService _analyticsService;
        private readonly SignalBus _signalBus;
        private readonly EcsWorld _world;

        public StartToPlayController(SignalBus signalBus, EcsWorld world)
        {
            _signalBus = signalBus;
            _world = world;
        }
        
        public void Initialize()
        {
            // View.StartToPlay.OnClickAsObservable().Subscribe(x => OnStart()).AddTo(View.StartToPlay);
            View.Filler1Btn.OnClickAsObservable().Subscribe(x => SetFiller1()).AddTo(View.Filler1Btn);
            View.Filler2Btn.OnClickAsObservable().Subscribe(x => SetFiller2()).AddTo(View.Filler2Btn);
        }
        
        private void OnStart()
        {
            _world.GetEntity<PlayerComponent>().Get<LevelStartEventComponent>();
        }
        
        private void SetFiller1()
        {
            _world.GetEntity<BoxComponent>().Get<BoxComponent>().Filler = EBoxFiller.Filler_1;
            View.Filler1Enabled.gameObject.SetActive(true);
            CloseFillersWindow();
        }
        
        private void SetFiller2()
        {
            _world.GetEntity<BoxComponent>().Get<BoxComponent>().Filler = EBoxFiller.Filler_2;
            View.Filler2Enabled.gameObject.SetActive(true);
            CloseFillersWindow();
        }
        
        private void CloseFillersWindow()
        {
            View.transform.DOMove(Vector3.zero, 0.5f). SetRelative(true).OnComplete(() =>
            {
                View.Fillers.gameObject.SetActive(false);
                OnStart();
            });
        }
    }
}