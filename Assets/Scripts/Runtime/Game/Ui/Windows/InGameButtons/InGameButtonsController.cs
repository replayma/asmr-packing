using DG.Tweening;
using ECS.Game.Systems.GameCycle;
using ECS.Utils.Extensions;
using ECS.Views.GameCycle;
using Game.SceneLoading;
using Leopotam.Ecs;
using Runtime.DataBase.Game;
using Runtime.Game.Ui.Windows.InGameMenu;
using Runtime.Services.CommonPlayerData;
using Runtime.Services.CommonPlayerData.Data;
using Runtime.Signals;
using SimpleUi.Abstracts;
using SimpleUi.Signals;
using UniRx;
using UnityEngine;
using Utils.UiExtensions;
using Zenject;

namespace Runtime.Game.Ui.Windows.InGameButtons
{
    public class InGameButtonsController : UiController<InGameButtonsView>, IInitializable
    {
        [Inject] private readonly ICommonPlayerDataService<CommonPlayerData> _commonPlayerData;
        [Inject] private readonly ISceneLoadingManager _sceneLoadingManager;
        private readonly SignalBus _signalBus;
        private EcsWorld _world; 

        private int _lastPrice = 0;
        private bool isFree = true;

        public InGameButtonsController(SignalBus signalBus, EcsWorld world)
        {
            _signalBus = signalBus;
            _world = world;
        }

        public void Initialize()
        {
            View.DropProgressButton.OnClickAsObservable().Subscribe(x => OnDropProggress()).AddTo(View.DropProgressButton);
            View.InGameMenuButton.OnClickAsObservable().Subscribe(x => OnGameMenu()).AddTo(View.InGameMenuButton);
            View.RollbackBtn.OnClickAsObservable().Subscribe(x => OnRollback()).AddTo(View.RollbackBtn);
            View.PutBtn.OnClickAsObservable().Subscribe(x => OnPut()).AddTo(View.PutBtn);
            View.FinishBtn.OnClickAsObservable().Subscribe(x => OnFinish()).AddTo(View.FinishBtn);
            
            View.Packing1Button.OnClickAsObservable().Subscribe(x => SetPacker1()).AddTo(View.Packing1Button);
            View.Packing2Button.OnClickAsObservable().Subscribe(x => SetPacker2()).AddTo(View.Packing2Button);
            View.Packing3Button.OnClickAsObservable().Subscribe(x => SetPacker3()).AddTo(View.Packing3Button);
            View.Packing4Button.OnClickAsObservable().Subscribe(x => SetPacker4()).AddTo(View.Packing4Button);
            
            View.Sticker1Button.OnClickAsObservable().Subscribe(x => SetSticker1()).AddTo(View.Sticker1Button);

            _signalBus.GetStream<SignalJoystickUpdate>().Subscribe(x => View.UpdateJoystick(ref x)).AddTo(View);
            // _signalBus.GetStream<SignalHpBarUpdate>().Subscribe(x => View.UpdateHpBar(ref x)).AddTo(View);
            // _signalBus.GetStream<SignalLifeCountUpdate>().Subscribe(x => View.UpdateLifeCount(ref x)).AddTo(View);
            // _signalBus.GetStream<SignalScoreUpdate>().Subscribe(x => View.UpdateScore(ref x)).AddTo(View);
            _signalBus.GetStream<SignalLevelEnd>().Subscribe(x => View.SetFinishBtn(ref x.Value)).AddTo(View);
            _signalBus.GetStream<SignalUpdateCurrency>().Subscribe(x => View.UpdateCurrency(ref x.Value)).AddTo(View);
            _signalBus.GetStream<SignalLevelState>().Subscribe(x => View.UpdateLevelState(ref x.State)).AddTo(View);
        }

        public void OnDropProggress()
        {
            var data = _commonPlayerData.GetData();
            data.Level = EScene.Level_01;
            _commonPlayerData.Save(data);
            _sceneLoadingManager.LoadScene(EScene.Level_01);
        }
        
        public override void OnShow()
        {
            View.Show(_commonPlayerData.GetData());
        }

        private void OnGameMenu()
        {
            _signalBus.OpenWindow<InGameMenuWindow>();
            _world.SetStage(EGameStage.Pause);
        }

        private void OnRollback()
        {
            var data = _commonPlayerData.GetData();
            var delta = data.Money - _lastPrice;
            data.Money = delta < 0 ? 0 : delta;
            _signalBus.Fire(new SignalUpdateCurrency(data.Money));
            _commonPlayerData.Save(data);

            if (isFree)
            {
                _lastPrice = 50;
                isFree = false;
            }
            else
                _lastPrice = Mathf.RoundToInt(_lastPrice * 1.2f);

            View.RollbackPrice.text = _lastPrice.ToString();

            View.RollbackBtn.gameObject.SetActive(false);
            View.PutBtn.gameObject.SetActive(true);
            _world.GetEntity<GameStateComponent>().Get<GameStateComponent>().State = EGameState.Rollback;
        }

        private void OnPut()
        {
            View.RollbackBtn.gameObject.SetActive(true);
            View.PutBtn.gameObject.SetActive(false);
            _world.GetEntity<GameStateComponent>().Get<GameStateComponent>().State = EGameState.Put;
        }

        private void OnFinish()
        {
            _world.SetStage(EGameStage.Complete);
        }
        
        // If somebody see this, don't judge me. I haven't time to develop pure code.
        private void SetPacker1()
        {
            _world.GetEntity<BoxComponent>().Get<BoxComponent>().Package = EBoxPackage.Package_1;
            View.Packing1Enabled.gameObject.SetActive(true);
            ClosePackersWindow();
        }
        
        private void SetPacker2()
        {
            _world.GetEntity<BoxComponent>().Get<BoxComponent>().Package = EBoxPackage.Package_2;
            View.Packing2Enabled.gameObject.SetActive(true);
            ClosePackersWindow();
        }
        
        private void SetPacker3()
        {
            _world.GetEntity<BoxComponent>().Get<BoxComponent>().Package = EBoxPackage.Package_3;
            View.Packing3Enabled.gameObject.SetActive(true);
            ClosePackersWindow();
        }
        
        private void SetPacker4()
        {
            _world.GetEntity<BoxComponent>().Get<BoxComponent>().Package = EBoxPackage.Package_4;
            View.Packing4Enabled.gameObject.SetActive(true);
            ClosePackersWindow();
        }
        
        private void ClosePackersWindow()
        {
            View.transform.DOMove(Vector3.zero, 0.5f). SetRelative(true).OnComplete(() =>
            {
                View.BeforePacking.gameObject.SetActive(false);
                View.UiBox.gameObject.SetActive(true);
                _world.GetEntity<LevelStateComponent>().Get<EventChangeLevelStateComponent>().State =
                    ELevelState.AfterPacking;
            });
        }
        
        private void SetSticker1()
        {
            _world.GetEntity<BoxComponent>().Get<BoxComponent>().Sticker = EBoxSticker.Sticker_1;
            View.Sticker1Enabled.gameObject.SetActive(true);
            CloseStickersWindow();
        }

        private void CloseStickersWindow()
        {
            View.transform.DOMove(Vector3.zero, 0.5f). SetRelative(true).OnComplete(() =>
            {
                View.BeforeSticker.gameObject.SetActive(false);
                View.UiBox.gameObject.SetActive(true);
                _world.GetEntity<LevelStateComponent>().Get<EventChangeLevelStateComponent>().State =
                    ELevelState.AfterStiker;
            });
        }
    }
}