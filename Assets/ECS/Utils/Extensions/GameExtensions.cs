﻿using ECS.Game.Components;
using ECS.Game.Components.Flags;
using ECS.Game.Components.General;
using ECS.Game.Systems.GameCycle;
using ECS.Views.GameCycle;
using Leopotam.Ecs;
using Runtime.Game.Ui.Windows.TouchPad;
using Services.Uid;
using UnityEngine;
using Object = UnityEngine.Object;

namespace ECS.Utils.Extensions
{
    public static class GameExtensions
    {
        public static void CreateEcsEntities(this EcsWorld world)
        {
            world.CreateCamera();
            world.CreateBox();
            world.CreatePutables();
            world.CreateDistanceTriggers();
        }
        
        public static void GetInput(this EcsWorld world, out EcsEntity inputEntity)
        {
            inputEntity = world.GetEntity<InputComponent>();
            if (inputEntity.IsNull())
            {
                var entity = world.NewEntity();
                entity.Get<InputComponent>();
                inputEntity = entity;
            }
        }
        
        public static void CreateCamera(this EcsWorld world)
        {
            var entity = world.NewEntity();
            entity.Get<UIdComponent>().Value = UidGenerator.Next();
            entity.GetAndFire<CameraComponent>();
            entity.Get<PlayerComponent>();
            entity.Get<ScoreComponent>();
            entity.Get<ScoreComponent>();
            entity.Get<GameStateComponent>().State = EGameState.Put;
            entity.GetAndFire<PrefabComponent>().Value = "Camera";
        }

        public static void CreateBox(this EcsWorld world)
        {
            var view = Object.FindObjectOfType<BoxView>(true);
            var entity = world.NewEntity();
            entity.Get<UIdComponent>().Value = UidGenerator.Next();
            entity.Get<BoxComponent>();
            entity.LinkView(view);
        }

        public static void CreatePutables(this EcsWorld world)
        {
            var views = Object.FindObjectsOfType<PutableView>(true);
            foreach (var view in views)
            {
                var entity = world.NewEntity();
                entity.Get<UIdComponent>().Value = UidGenerator.Next();
                entity.Get<PutableComponent>();
                entity.LinkView(view);
            }
        }
        
        public static void CreateSticker(this EcsWorld world, string prefabName)
        {
            var entity = world.NewEntity();
            entity.GetAndFire<PrefabComponent>().Value = prefabName;
            entity.Get<StickerComponent>();
            entity.Get<EcsDisableComponent>();
        }
        
        public static void CreateParticle(this EcsWorld world, string prefabName, Vector3 position, float delay = 5f)
        {
            var entity = world.NewEntity();
            entity.GetAndFire<PrefabComponent>().Value = prefabName;
            entity.GetAndFire<ParticleComponent>().Pos = position;
            entity.Get<IsDelayCleanUpComponent>().Delay = delay;
        }

        public static void CreateDistanceTriggers(this EcsWorld world)
        {
            var views = Object.FindObjectsOfType<DistanceTriggerView>(true);
            foreach (var view in views)
            {
                var entity = world.NewEntity();
                entity.Get<UIdComponent>().Value = UidGenerator.Next();
                entity.Get<DistanceTriggerComponent>();
                entity.LinkView(view);
            }
        }
    }
}