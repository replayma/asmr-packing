﻿using ECS.Views.Impls;
using Leopotam.Ecs;
using UnityEngine;

namespace ECS.Views.GameCycle
{
    public class PutableView : LinkableView
    {
        public ref bool IsPutAboveFiller() => ref _putAboveFiller;
        [SerializeField] private bool _putAboveFiller;
        
        public ref BoxCollider GetBoxCollider() => ref _boxCollider;
        [SerializeField] private BoxCollider _boxCollider;
        public ref BoxCollider GetTopCollider() => ref _topCollider;
        [SerializeField] private BoxCollider _topCollider;
        // public ref float GetConnectionDistance() => ref _connectionDistance;
        // [SerializeField] private float _connectionDistance;
        public ref Transform GetTablePosition() => ref _tablePosition;
        [SerializeField] private Transform _tablePosition;
        public ref Rigidbody GetPivot() => ref _pivot;
        [SerializeField] private Rigidbody _pivot;
        public ref MeshRenderer GetMeshRenderer() => ref _meshRenderer;
        [SerializeField] private MeshRenderer _meshRenderer;

        public ref Material GetApproveMaterial() => ref _approveMaterial;
        [SerializeField] private Material _approveMaterial;
        public ref Material GetRejectMaterial() => ref _rejectMaterial;
        [SerializeField] private Material _rejectMaterial;
        public ref Material GetOriginMaterial() => ref _originMaterial;
        private Material _originMaterial;

        public override void Link(EcsEntity entity)
        {
            base.Link(entity);
            _originMaterial = _meshRenderer.material;
        }
    }

    public struct PutableComponent : IEcsIgnoreInFilter
    {
    }
}