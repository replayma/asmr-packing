﻿using ECS.Views.Impls;
using Leopotam.Ecs;
using UnityEngine;

namespace ECS.Views.GameCycle
{
    public class StickerView : LinkableView
    {
        public ref MeshRenderer GetMeshRenderer() => ref _meshRenderer;
        [SerializeField] private MeshRenderer _meshRenderer;
        public ref Material GetApproveMaterial() => ref _approveMaterial;
        [SerializeField] private Material _approveMaterial;
        public ref Material GetOriginMaterial() => ref _originMaterial;
        private Material _originMaterial;

        public override void Link(EcsEntity entity)
        {
            base.Link(entity);
            _originMaterial = _meshRenderer.material;
        }
    }

    public struct StickerComponent : IEcsIgnoreInFilter
    {
    }
}