﻿using ECS.Views.Impls;
using Leopotam.Ecs;
using UnityEngine;

namespace ECS.Views.GameCycle
{
    public class BoxView : LinkableView
    {
        [SerializeField] private int _width;
        [SerializeField] private int _lenght;
        [SerializeField] private Transform _gridPivot;
        
        public ref float GetStep() => ref _step;
        [SerializeField] private float _step;

        public ref Transform GetFiller1() => ref _filler1;
        [SerializeField] private Transform _filler1;
        public ref Transform GetFiller2() => ref _filler2;
        [SerializeField] private Transform _filler2;
        public ref Transform GetFiller1Particle() => ref _filler1Particle;
        [SerializeField] private Transform _filler1Particle;
        public ref Transform GetFiller2Particle() => ref _filler2Particle;
        [SerializeField] private Transform _filler2Particle;
        public ref Transform GetFillerHeight() => ref _fillerHeight;
        [SerializeField] private Transform _fillerHeight;
        public ref Transform GetFillerTopHeight() => ref _fillerTopHeight;
        [SerializeField] private Transform _fillerTopHeight;
        
        public ref Transform GetPackage1() => ref _package1;
        [SerializeField] private Transform _package1;
        public ref Transform GetPackage2() => ref _package2;
        [SerializeField] private Transform _package2;
        public ref Transform GetPackage3() => ref _package3;
        [SerializeField] private Transform _package3;
        public ref Transform GetPackage4() => ref _package4;
        [SerializeField] private Transform _package4;
        
        public ref Transform GetFront() => ref _front;
        [SerializeField] private Transform _front;
        public ref Transform GetLeft() => ref _left;
        [SerializeField] private Transform _left;
        public ref Transform GetLeftFinish() => ref _leftFinish;
        [SerializeField] private Transform _leftFinish;
        public ref Transform GetRight() => ref _right;
        [SerializeField] private Transform _right;
        public ref Transform GetRightFinish() => ref _rightFinish;
        [SerializeField] private Transform _rightFinish;

        private Vector3[,] _grid;
        
        public ref Vector3[,] GetGrid() => ref _grid;
        
        public override void Link(EcsEntity entity)
        {
            base.Link(entity);

            var width = _width - 1;
            var lenght = _lenght - 1;
            
            _grid = new Vector3[width, lenght];
            for (int i = 0; i < width; i++)
                for (int j = 0; j < lenght; j++)
                    _grid[i, j] = _gridPivot.position + new Vector3(_step * (i + 1), 0, _step * (j + 1));
        }

        public void OnDrawGizmos()
        {
            if (_grid == null)
                return;
            
            foreach (var point in _grid)
            {
                Gizmos.color = Color.magenta;
                Gizmos.DrawSphere(point, 0.05f);
            }
        }
    }

    public struct BoxComponent
    {
        public EBoxFiller Filler;
        public EBoxPackage Package;
        public EBoxSticker Sticker;
    }

    public enum EBoxFiller
    {
        Filler_1,
        Filler_2
    }
    
    public enum EBoxPackage
    {
        Package_1,
        Package_2,
        Package_3,
        Package_4
    }
    
    public enum EBoxSticker
    {
        Sticker_1
    }
}