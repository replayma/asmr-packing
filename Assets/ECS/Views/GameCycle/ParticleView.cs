﻿using ECS.Views.Impls;
using Leopotam.Ecs;
using UnityEngine;

namespace ECS.Views.GameCycle
{
    public class ParticleView : LinkableView
    {
        
    }

    public struct ParticleComponent : IEcsIgnoreInFilter
    {
        public Vector3 Pos;
    }
}