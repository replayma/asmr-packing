﻿using ECS.Core.Utils.ReactiveSystem;
using ECS.Core.Utils.ReactiveSystem.Components;
using ECS.Game.Components.General;
using ECS.Views.GameCycle;
using Leopotam.Ecs;

namespace ECS.Game.Systems.GameCycle
{
    public class ParticleSpawnSystem : ReactiveSystem<EventAddComponent<ParticleComponent>>
    {
#pragma warning disable 649
#pragma warning restore 649
        protected override EcsFilter<EventAddComponent<ParticleComponent>> ReactiveFilter { get; }
        protected override bool DeleteEvent => true;

        protected override void Execute(EcsEntity entity)
        {
            entity.Get<LinkComponent>().View.Transform.position = entity.Get<ParticleComponent>().Pos;
        }
    }
}