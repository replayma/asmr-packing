﻿using System.Diagnostics.CodeAnalysis;
using DG.Tweening;
using ECS.Core.Utils.ReactiveSystem;
using ECS.Game.Components.Flags;
using ECS.Game.Components.General;
using ECS.Utils.Extensions;
using ECS.Views.GameCycle;
using ECS.Views.Impls;
using Leopotam.Ecs;
using Runtime.DataBase.Game;
using Runtime.Game.Ui;
using Runtime.Game.Utils.MonoBehUtils;
using Runtime.Services.AnalyticsService;
using SimpleUi.Signals;
using UniRx;
using UnityEngine;
using Zenject;

// ReSharper disable All
#pragma warning disable 649

namespace ECS.Game.Systems.GameCycle
{
    [SuppressMessage("ReSharper", "PossibleNullReferenceException")]
    public class LevelStartSystem : ReactiveSystem<LevelStartEventComponent>
    {
        [Inject] private IAnalyticsService _analyticsService;
        [Inject] private SignalBus _signalBus;
        [Inject] private ScreenVariables _screenVariables;
        private const string CAMERA_START = "CameraStart";
        private const string OBJECTS = "Objects";

        private int SCORE_FOR_EACH_OBJECT;

        private readonly EcsWorld _world;
        private readonly EcsFilter<BoxComponent, LinkComponent> _box;
        private readonly EcsFilter<CameraComponent, LinkComponent> _cameraF;

        private readonly CompositeDisposable _disposable = new CompositeDisposable();
        private bool started = false;
        private CameraView _cameraView;
        private EcsEntity _playerEntity;

        // ReSharper disable once UnassignedGetOnlyAutoProperty
        protected override EcsFilter<LevelStartEventComponent> ReactiveFilter { get; }
        protected override bool DeleteEvent => true;

        protected override void Execute(EcsEntity entity)
        {
            if (started)
                return;

            _world.SetStage(EGameStage.Play);
            _signalBus.OpenWindow<GameHudWindow>();
            _analyticsService.SendRequest("level_start");

            var objects = _screenVariables.GetTransformPoint(OBJECTS);
            objects.DOMove(objects.position - Vector3.right * 7, 0.7f).SetEase(Ease.Flash);
            
            InitCameraTween();
            started = true;
        }

        private void InitCameraTween()
        {
            
            foreach (var i in _cameraF)
            {
                _cameraF.GetEntity(i).Get<EventChangeLevelStateComponent>().State = ELevelState.Putting;
                var cameraView = _cameraF.Get2(i).Get<CameraView>();
                cameraView.Transform.DOMove(_screenVariables.GetTransformPoint(CAMERA_START).position, 1f)
                    .SetEase(Ease.Linear).SetDelay(0.15f)
                .OnComplete(() =>
                    {
                        var clue = _screenVariables.GetTransformPoint("FirstClue");
                        clue.transform.DOMove(Vector3.zero, 0.4f).SetRelative(true).OnComplete(() => clue.gameObject.SetActive(true));
                        // InitBox();
                    });
            }
        }

        // private void InitBox()
        // {
        //     foreach (var i in _box)
        //     {
        //         var view = _box.Get2(i).Get<BoxView>();
        //         
        //         switch (_box.Get1(i).Filler)
        //         {
        //             case EBoxFiller.Filler_1:
        //                 view.GetFiller1().gameObject.SetActive(true);
        //                 view.GetFiller1().DOMoveY(view.GetFillerHeight().position.y, 1.5f).SetEase(Ease.Linear).SetDelay(0.3f);
        //                 break;
        //             case EBoxFiller.Filler_2:
        //                 view.GetFiller2().gameObject.SetActive(true);
        //                 view.GetFiller2().DOMoveY(view.GetFillerHeight().position.y, 1.5f).SetEase(Ease.Linear).SetDelay(0.3f);
        //                 break;
        //         }
        //     }
        // }
    }

    public struct LevelStartEventComponent : IEcsIgnoreInFilter
    {
    }
}