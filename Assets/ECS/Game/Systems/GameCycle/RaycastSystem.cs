﻿using ECS.Core.Utils.SystemInterfaces;
using ECS.Game.Components.Flags;
using ECS.Game.Components.General;
using ECS.Utils.Extensions;
using ECS.Views.GameCycle;
using ECS.Views.Impls;
using Leopotam.Ecs;
using Runtime.Game.Utils.MonoBehUtils;
using Runtime.Services.VibrationService;
using Runtime.Signals;
using UnityEngine;
using Zenject;

namespace ECS.Game.Systems.GameCycle
{
    public class RaycastSystem : IEcsUpdateSystem
    {
        [Inject] private SignalBus _signalBus;
        [Inject] private ScreenVariables _screenVariables;
        [Inject] private IVibrationService _vibrationService;
        
#pragma warning disable
        private readonly EcsWorld _world;
        private readonly EcsFilter<GameStateComponent> _state;
        private readonly EcsFilter<CameraComponent, LinkComponent> _cameraF;
        private readonly EcsFilter<BoxComponent, LinkComponent> _box;
        private readonly EcsFilter<EventInputDownComponent> _eventDown;
        private readonly EcsFilter<EventInputHoldAndDragComponent> _eventHoldAndDrag;
        private readonly EcsFilter<EventInputUpComponent> _eventUp;
        private readonly EcsFilter<PutableComponent, LinkComponent, StageInPutComponent> _inPut;
        private readonly EcsFilter<PutableComponent, LinkComponent>.Exclude<OnPlaceComponent> _remaining;
#pragma warning restore 649

        private CameraView _cameraView;
        private Camera _camera;

        private PutableView _putableView;
        private EcsEntity _putableEntity;

        private BoxView _boxView;


        private int _accessMask = LayerMask.GetMask("Putable", "Default");
        private int _putableMask = LayerMask.GetMask("Putable");
        private int _surfaceMask = LayerMask.GetMask("Surface");
        private int _defaultMask = LayerMask.GetMask("Default");
        private RaycastHit _hit;
        private Collider[] _colliders = new Collider[8];
        private Vector3 _collisionTolerance = new Vector3(0.01f, 0.01f, 0.01f);
        private Vector2 _targetPositionRaycastOffset = new Vector2(0, 100f);
        // private Vector2 _targetPositionRaycastOffset = new Vector2(0, 0f);

        private Vector3 _cachePoint;
        private bool _stopInput;

        private SignalJoystickUpdate _signalJoystickUpdate =
            new SignalJoystickUpdate(false, Vector2.zero, Vector2.zero);

        public void Run()
        {
            foreach (var i in _cameraF)
            {
                if (_cameraF.GetEntity(i).Get<LevelStateComponent>().State != ELevelState.Putting)
                    return;
            }
            
            foreach (var i in _state)
                if (_state.Get1(i).State == EGameState.Rollback)
                    return;

            foreach (var i in _cameraF)
            {
                _cameraView = _cameraF.Get2(i).Get<CameraView>();
                _camera = _cameraView.GetCamera();
            }

            foreach (var i in _box)
                _boxView = _box.Get2(i).Get<BoxView>();

            foreach (var i in _eventDown)
            {
                if (TryCameraRaycast(_eventDown.Get1(i).Down, ref _putableMask))
                {
                    _putableView = _hit.collider.gameObject.GetComponentInParent<PutableView>();
                    _putableEntity = _putableView.Entity;
                    _stopInput = _putableEntity.Has<OnPlaceComponent>();
                }
                else
                    _stopInput = true;
                
                _screenVariables.GetTransformPoint("FirstClue").gameObject.SetActive(false);
                _eventDown.GetEntity(i).Del<EventInputDownComponent>();
            }

            if (_stopInput)
                return;
            
            foreach (var i in _eventHoldAndDrag)
            {
                if (_putableView == null)
                    break;
                if (TryCameraRaycast(_eventHoldAndDrag.Get1(i).Drag + _targetPositionRaycastOffset, ref _surfaceMask))
                {
                    _putableView.GetPivot().position = _hit.point;
                    foreach (var pos in _boxView.GetGrid())
                    {
                        if (Vector2.Distance(new Vector2(pos.x, pos.z),
                                new Vector2(_putableView.GetPivot().position.x, _putableView.GetPivot().position.z)) <=
                            _boxView.GetStep())
                        {
                            // if (_putableView.IsPutAboveFiller() && _hit.point.y < _boxView.GetFillerHeight().position.y + 0.523f)
                            //     _cachePoint = new Vector3(pos.x, _boxView.GetFillerHeight().position.y + 0.523f, pos.z);
                            // else
                                _cachePoint = new Vector3(pos.x, _hit.point.y, pos.z);
                            _putableView.GetPivot().position = _cachePoint;
                            _putableView.GetBoxCollider().gameObject.layer = LayerMask.NameToLayer("Ignore Raycast");
                            _putableView.GetTopCollider().gameObject.layer = LayerMask.NameToLayer("Ignore Raycast");
                            break;
                        }
                    }

                    if (_putableView.GetPivot().position == _cachePoint)
                    {
                        if (Physics.OverlapBoxNonAlloc(
                            _putableView.GetPivot().transform.TransformPoint(_putableView.GetBoxCollider().center +
                                                                             _putableView.GetBoxCollider().transform
                                                                                 .localPosition),
                            _putableView.GetBoxCollider().size / 2 - _collisionTolerance, _colliders,
                            Quaternion.identity,
                            _accessMask) > 0)
                            _putableEntity.Get<StageInPutComponent>().Stage = EInPutStage.Reject;
                        else
                            _putableEntity.Get<StageInPutComponent>().Stage = EInPutStage.Approve;
                    }
                    else
                        _putableEntity.Get<StageInPutComponent>().Stage = EInPutStage.Origin;
                }
                else if (TryCameraRaycast(_eventHoldAndDrag.Get1(i).Drag + _targetPositionRaycastOffset, ref _defaultMask))
                {
                    _putableView.GetPivot().position = _hit.point + Vector3.up;
                    _putableEntity.Get<StageInPutComponent>().Stage = EInPutStage.InPut;
                }
                
                ClearColliders();
                _eventHoldAndDrag.GetEntity(i).Del<EventInputHoldAndDragComponent>();
            }

            foreach (var i in _eventUp)
            {
                if (_putableView == null)
                    break;
                foreach (var j in _inPut)
                {
                    switch (_inPut.Get3(j).Stage)
                    {
                        case EInPutStage.Approve:
                            _putableEntity.Get<StageInPutComponent>().Stage = EInPutStage.OnPlace;
                            break;
                        default:
                            _putableEntity.Get<StageInPutComponent>().Stage = EInPutStage.Origin;
                            break;
                    }
                }
                _eventUp.GetEntity(i).Del<EventInputUpComponent>();
            }

            foreach (var i in _inPut)
            {
                var putableView = _inPut.Get2(i).Get<PutableView>();
                var putableEntity = _inPut.GetEntity(i);
                switch (_inPut.Get3(i).Stage)
                {
                    case EInPutStage.Approve:
                        putableView.GetMeshRenderer().material = putableView.GetApproveMaterial();
                        break;
                    case EInPutStage.Reject:
                        putableView.GetMeshRenderer().material = putableView.GetRejectMaterial();
                        break;
                    case EInPutStage.Origin:
                        putableView.GetMeshRenderer().material = putableView.GetOriginMaterial();
                        putableView.GetBoxCollider().gameObject.layer = LayerMask.NameToLayer("Putable");
                        putableView.GetTopCollider().gameObject.layer = LayerMask.NameToLayer("Ignore Raycast");
                        putableView.GetPivot().position = putableView.GetTablePosition().position;
                        _cachePoint = Vector3.zero;
                        putableEntity.Del<StageInPutComponent>();
                        break;
                    case EInPutStage.InPut:
                        putableView.GetMeshRenderer().material = putableView.GetOriginMaterial();
                        break;
                    case EInPutStage.OnPlace:
                        putableView.GetMeshRenderer().material = putableView.GetOriginMaterial();
                        putableView.GetBoxCollider().gameObject.layer = LayerMask.NameToLayer("Putable");
                        putableView.GetTopCollider().gameObject.layer = LayerMask.NameToLayer("Surface");
                        _world.CreateParticle("ConfettiDirectional", putableView.GetPivot().position);
                        _vibrationService.Vibrate(120);
                        _cachePoint = Vector3.zero;
                        putableEntity = _inPut.GetEntity(i);
                        putableEntity.Get<OnPlaceComponent>();
                        putableEntity.Del<StageInPutComponent>();
                        break;
                }
                // _signalBus.Fire(new SignalLevelEnd(_remaining.GetEntitiesCount() <= 0));
                if (_remaining.GetEntitiesCount() <= 0)
                    foreach (var j in _cameraF)
                        _cameraF.GetEntity(j).Get<EventChangeLevelStateComponent>().State = ELevelState.BeforePacking;
            }
        }

        private void ClearColliders()
        {
            for (int i = 0; i < _colliders.Length; i++)
                if (_colliders[0] != null)
                {
                    // Debug.Log(_colliders[0].name);
                    _colliders[0] = null;
                }
        }

        private bool TryCameraRaycast(Vector2 point, ref int layerMask) =>
            Physics.Raycast(_camera.ScreenPointToRay(point), out _hit, 100f, layerMask);
    }

    public struct StageInPutComponent
    {
        public EInPutStage Stage;
    }

    public enum EInPutStage
    {
        Origin,
        Approve,
        Reject,
        OnPlace,
        InPut
    }
}