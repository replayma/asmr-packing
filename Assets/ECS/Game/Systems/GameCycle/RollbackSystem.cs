﻿using ECS.Core.Utils.SystemInterfaces;
using ECS.Game.Components.Flags;
using ECS.Game.Components.General;
using ECS.Views.GameCycle;
using ECS.Views.Impls;
using Leopotam.Ecs;
using Runtime.Services.VibrationService;
using Runtime.Signals;
using UnityEngine;
using Zenject;

namespace ECS.Game.Systems.GameCycle
{
    public class RollbackSystem : IEcsUpdateSystem
    {
        [Inject] private SignalBus _signalBus;
        [Inject] private IVibrationService _vibrationService;
        
#pragma warning disable
        private readonly EcsWorld _world;
        private readonly EcsFilter<GameStateComponent> _state;
        private readonly EcsFilter<CameraComponent, LinkComponent> _cameraF;
        private readonly EcsFilter<EventInputDownComponent> _eventDown;
        private readonly EcsFilter<EventInputHoldAndDragComponent> _eventHoldAndDrag;
        private readonly EcsFilter<EventInputUpComponent> _eventUp;
        private readonly EcsFilter<PutableComponent, LinkComponent, OnPlaceComponent> _onPlaces;
        private readonly EcsFilter<PutableComponent, LinkComponent>.Exclude<OnPlaceComponent> _remaining;
#pragma warning restore 649

        private CameraView _cameraView;
        private Camera _camera;
        private EcsEntity _cameraEntity;

        private PutableView _putableView;
        private EcsEntity _putableEntity;

        private BoxView _boxView;
        private EcsEntity _boxEntity;

        private int _putableMask = LayerMask.GetMask("Putable");
        private RaycastHit _hit;
        public void Run()
        {
            foreach (var i in _cameraF)
            {
                if (_cameraF.GetEntity(i).Get<LevelStateComponent>().State != ELevelState.Putting)
                    return;
            }
            
            foreach (var i in _state)
                if (_state.Get1(i).State == EGameState.Put)
                    return;
            
            foreach (var i in _cameraF)
            {
                _cameraView = _cameraF.Get2(i).Get<CameraView>();
                _camera = _cameraView.GetCamera();
                _cameraEntity = _cameraF.GetEntity(i);
            }
            
            foreach (var i in _eventDown)
            {
                if (TryCameraRaycast(_eventDown.Get1(i).Down, ref _putableMask))
                {
                    _putableView = _hit.collider.gameObject.GetComponentInParent<PutableView>();
                    _putableEntity = _putableView.Entity;
                    if (!_putableEntity.Has<OnPlaceComponent>())
                        return;
                    _putableView.GetPivot().position = _putableView.GetTablePosition().position;
                    _putableView.GetBoxCollider().gameObject.layer = LayerMask.NameToLayer("Putable");
                    _putableView.GetTopCollider().gameObject.layer = LayerMask.NameToLayer("Ignore Raycast");
                    _putableEntity.Del<OnPlaceComponent>();
                    _vibrationService.Vibrate(120);
                }

                _eventDown.GetEntity(i).Del<EventInputDownComponent>();
                
                // _signalBus.Fire(new SignalLevelEnd(_remaining.GetEntitiesCount() <= 0));
            }

            foreach (var i in _eventHoldAndDrag)
                _eventHoldAndDrag.GetEntity(i).Del<EventInputHoldAndDragComponent>();
            foreach (var i in _eventUp)
                _eventUp.GetEntity(i).Del<EventInputUpComponent>();
        }
        
        private bool TryCameraRaycast(Vector2 point, ref int layerMask) =>
            Physics.Raycast(_camera.ScreenPointToRay(point), out _hit, 100f, layerMask);
    }

    public struct OnPlaceComponent : IEcsIgnoreInFilter
    {
    }

    public struct GameStateComponent
    {
        public EGameState State;
    }

    public enum EGameState
    {
        Put,
        Rollback
    }
    
}