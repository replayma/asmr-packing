﻿using DG.Tweening;
using ECS.Core.Utils.ReactiveSystem;
using ECS.Game.Components.Flags;
using ECS.Game.Components.General;
using ECS.Utils.Extensions;
using ECS.Views.GameCycle;
using Leopotam.Ecs;
using Runtime.DataBase.Game;
using Runtime.Game.Utils.MonoBehUtils;
using Runtime.Signals;
using UnityEngine;
using Zenject;

namespace ECS.Game.Systems.GameCycle
{
    public class LevelStateSystem : ReactiveSystem<EventChangeLevelStateComponent>
    {
        [Inject] private SignalBus _signalBus;
        [Inject] private readonly ScreenVariables _screenVariables;
        
        private readonly EcsWorld _world;
        private readonly EcsFilter<CameraComponent, LinkComponent> _cameraF;
        private readonly EcsFilter<BoxComponent, LinkComponent> _box;
        private readonly EcsFilter<PutableComponent, LinkComponent> _putables;
        protected override EcsFilter<EventChangeLevelStateComponent> ReactiveFilter { get; }
        protected override bool DeleteEvent => true;
        protected override void Execute(EcsEntity entity)
        {
            entity.Get<LevelStateComponent>().State = entity.Get<EventChangeLevelStateComponent>().State;

            switch (entity.Get<LevelStateComponent>().State)
            {
                case ELevelState.BeforePacking:
                    foreach (var i in _box)
                    {
                        var view = _box.Get2(i).Get<BoxView>();
                        view.GetFront().GetComponent<MeshRenderer>().material =
                            new Material(view.GetLeft().GetComponent<MeshRenderer>().material);
                        var pos = _screenVariables.GetTransformPoint("BoxFinal").position;
                        _world.CreateParticle("ConfettiBlast", view.GetFillerTopHeight().position + Vector3.up, 1.5f);
                        view.GetLeft().DORotateQuaternion(view.GetLeftFinish().rotation, 0.3f).SetEase(Ease.Linear);
                        view.GetRight().DORotateQuaternion(view.GetRightFinish().rotation, 0.3f).SetEase(Ease.Linear)
                            .OnComplete(() =>
                            {
                                foreach (var j in _putables)
                                    _putables.Get2(j).View.Transform.SetParent(view.Transform);
                            });
                        view.Transform.DOMove(pos, 0.6f)
                            .SetEase(Ease.Linear).SetDelay(0.6f)
                            .OnComplete(() =>
                            {
                                _signalBus.Fire(new SignalLevelState(ELevelState.BeforePacking));
                            });
                    }
                    break;
                case ELevelState.AfterPacking:
                    foreach (var i in _box)
                    {
                        var view = _box.Get2(i).Get<BoxView>();
                        view.Transform.DOMove(Vector3.zero, 0.6f).SetRelative(true).OnComplete(() =>
                        {
                            _world.CreateParticle("AfterPackingParticle", view.transform.position + Vector3.up);
                            switch (_box.Get1(i).Package)
                            {
                                case EBoxPackage.Package_1:
                                    view.GetPackage1().gameObject.SetActive(true);
                                    break;
                                case EBoxPackage.Package_2:
                                    view.GetPackage2().gameObject.SetActive(true);
                                    break;
                                case EBoxPackage.Package_3:
                                    view.GetPackage3().gameObject.SetActive(true);
                                    break;
                                case EBoxPackage.Package_4:
                                    view.GetPackage4().gameObject.SetActive(true);
                                    break;
                            }
                        });
                        view.Transform.DOMove(Vector3.zero, 0f).SetDelay(3f).SetRelative(true).OnComplete(() => 
                                _signalBus.Fire(new SignalLevelState(ELevelState.BeforeStiker)));
                        
                    }
                    break;
                case ELevelState.AfterStiker:
                    _world.CreateSticker("Sticker");
                    var clue = _screenVariables.GetTransformPoint("SecondClue");
                    clue.transform.DOMove(Vector3.zero, 0.4f).SetRelative(true).OnComplete(() => clue.gameObject.SetActive(true));
                    break;
                
                case ELevelState.End:
                    _world.SetStage(EGameStage.Complete);
                    break;
            }
        }
    }

    public struct EventChangeLevelStateComponent
    {
        public ELevelState State;
    }
    
    public struct LevelStateComponent
    {
        public ELevelState State;
    }

    public enum ELevelState
    {
        Putting,
        BeforePacking,
        AfterPacking,
        BeforeStiker,
        AfterStiker,
        End
    }
}