﻿using DG.Tweening;
using ECS.Core.Utils.SystemInterfaces;
using ECS.Game.Components.Flags;
using ECS.Game.Components.General;
using ECS.Utils.Extensions;
using ECS.Views.GameCycle;
using ECS.Views.Impls;
using Leopotam.Ecs;
using Runtime.DataBase.Game;
using Runtime.Game.Utils.MonoBehUtils;
using UnityEngine;
using Zenject;

namespace ECS.Game.Systems.GameCycle
{
    public class StickerSystem : IEcsUpdateSystem
    {
        [Inject] private ScreenVariables _screenVariables;
        
#pragma warning disable
        private readonly EcsWorld _world;
        private readonly EcsFilter<CameraComponent, LinkComponent> _cameraF;
        private readonly EcsFilter<BoxComponent, LinkComponent> _box;
        private readonly EcsFilter<EventInputDownComponent> _eventDown;
        private readonly EcsFilter<EventInputHoldAndDragComponent> _eventHoldAndDrag;
        private readonly EcsFilter<EventInputUpComponent> _eventUp;
        private readonly EcsFilter<StickerComponent, LinkComponent> _stickerDisable;
        private readonly EcsFilter<StickerComponent, LinkComponent>.Exclude<EcsDisableComponent> _sticker;
#pragma warning restore 649

        private CameraView _cameraView;
        private Camera _camera;

        private PutableView _putableView;
        private EcsEntity _putableEntity;

        private BoxView _boxView;
        private StickerView _stickerView;
        private int _mask = LayerMask.GetMask("StickerSurface");
        private RaycastHit _hit;
        private Vector2 _targetPositionRaycastOffset = new Vector2(0, 100f);

        public void Run()
        {
            foreach (var i in _cameraF)
            {
                if (_cameraF.GetEntity(i).Get<LevelStateComponent>().State != ELevelState.AfterStiker)
                    return;
            }

            foreach (var i in _cameraF)
            {
                _cameraView = _cameraF.Get2(i).Get<CameraView>();
                _camera = _cameraView.GetCamera();
            }

            foreach (var i in _box)
                _boxView = _box.Get2(i).Get<BoxView>();

            foreach (var i in _eventDown)
            {
                _screenVariables.GetTransformPoint("SecondClue").gameObject.SetActive(false);
                _eventDown.GetEntity(i).Del<EventInputDownComponent>();
            }

            foreach (var i in _eventHoldAndDrag)
            {
                if (TryCameraRaycast(_eventHoldAndDrag.Get1(i).Drag + _targetPositionRaycastOffset, ref _mask))
                {
                    foreach (var j in _stickerDisable)
                    {
                        _stickerView = _stickerDisable.Get2(j).Get<StickerView>();
                        _stickerView.Transform.position = _hit.point;
                        _stickerView.GetMeshRenderer().material = _stickerView.GetApproveMaterial();
                        _stickerView.Entity.Del<EcsDisableComponent>();
                    }
                }
                
                _eventHoldAndDrag.GetEntity(i).Del<EventInputHoldAndDragComponent>();
            }

            foreach (var i in _eventUp)
            {
                foreach (var j in _sticker)
                {
                    _stickerView = _sticker.Get2(j).Get<StickerView>();
                    _stickerView.GetMeshRenderer().material = _stickerView.GetOriginMaterial();
                    _sticker.GetEntity(j).Get<EcsDisableComponent>();
                    var pos = _stickerView.Transform.position;
                    _stickerView.Transform.position += Vector3.up * 7;
                    _stickerView.Transform.DOMove(pos, 0.4f).SetEase(Ease.Linear)
                        .OnComplete(() => _world.CreateParticle("StickerDust", pos));
                    _stickerView.Transform.DOMove(Vector3.zero, 1.6f).SetDelay(0.4f).SetRelative(true)
                        .OnComplete(() => _world.SetStage(EGameStage.Complete));
                }
                
                _eventUp.GetEntity(i).Del<EventInputUpComponent>();
            }
            
        }

        private bool TryCameraRaycast(Vector2 point, ref int layerMask) =>
            Physics.Raycast(_camera.ScreenPointToRay(point), out _hit, 100f, layerMask);
    }
}